<?php

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	die;
}

delete_option( 'acfci_settings' );
delete_option( 'ACFCI_cdn_error' );